#ifndef __SUMD__
#define __SUMD__

#define SUMD_SPEED 115200
#define SUMD_MAXCHAN 10

#define SUMD_BUFFSIZE SUMD_MAXCHAN * 2 + 5 // 8 channels + 5 -> 21 bytes for 8 channels

#endif