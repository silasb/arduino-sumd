# Arduino SUMD

A library that allows you to build SUMD packets.

It accepts 10 channels, but that can easily be customized by looking at `sumd.h` and `sumd.ino`.