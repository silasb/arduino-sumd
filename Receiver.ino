#include "sumd.h"

void setup() {
  Serial.begin(SUMD_SPEED);
}

void loop() {
  int roll, pitch, yaw, throttle = 0;
  int aux1, aux2, aux3, aux4, aux5, aux6 = 0;

  // set throttle to 512 of 1024.
  throttle = 512;

  uint8_t *sumd = BuildSumD(
    roll,
    pitch,
    yaw,
    throttle,
    aux1,
    aux2,
    aux3,
    aux4,
    aux5,
    aux6
  );

  Serial.write(sumd, SUMD_BUFFSIZE);
  delay(7);
}
